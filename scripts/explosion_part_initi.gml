///create_part_type_sprite(spr, blend, min_life, max_life, min_scale, max_scale, scaling)

var blend = argument1;
var spr = argument0;
var min_life = argument2;
var max_life = argument3;
var min_scale = argument4;
var max_scale = argument5;
var scaling = argument6;

//Explosion
pt_explosion = part_type_create();
part_type_alpha2(pt_explosion, .75, 0);
part_type_sprite(pt_explosion, spr, false, true, false);
part_type_blend(pt_explosion, blend);
part_type_size(pt_explosion, min_scale, max_scale, scaling, 0);
part_type_life(pt_explosion, min_life, max_life);
part_type_orientation(pt_explosion, 0, 360, 0, 0, 0);

return pt_explosion;