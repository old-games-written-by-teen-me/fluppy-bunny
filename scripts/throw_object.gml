///throw_object(x, y, dir, force, wiggle, obj, depth)
var xx = argument0;
var yy = argument1;
var dir = argument2;
var force = argument3;
var wiggle = argument4;
var obj = argument5;
var dept = argument6;

var mousedir = point_direction(x, y, mouse_x, mouse_y);
var p = instance_create(xx, yy, obj);
var off = 0;
if (wiggle) {
    off = random_range(-1, 1);
}
var xforce = lengthdir_x(force+off, dir);
var yforce = lengthdir_y(force+off, dir);
p.creator = id;
p.depth = dept;
with (p) {
    physics_apply_impulse(x, y, xforce, yforce);
}