//Get direction
{
    if (!obj_input.aim) obj_system.mouse_state = mouse_shooting;
    mouse_dir = point_direction(x, y, mouse_x, mouse_y);
    movement = MOVE;
    dir = point_direction(0, 0, obj_input.xaxis, obj_input.yaxis);
    //Get the length
    if (obj_input.xaxis == 0 and obj_input.yaxis == 0) && (able_to_move) {
        len = 0;
    } else {
        len = spd;
        get_face(dir);
    }
    
    //Get the hspd and vspd
    hspd = lengthdir_x (len, dir);
    vspd = lengthdir_y (len, dir);
    
    //Move
    if (able_to_move) {
        phy_position_x += hspd;
        phy_position_y += vspd;
    }
    
    //Control the sprite
    if !keyboard_check(vk_shift) {
        if (len == 0) image_index = 0;
            else image_speed = .3;
    } else {
        len = run_spd;
        image_speed = .6;
    }
}