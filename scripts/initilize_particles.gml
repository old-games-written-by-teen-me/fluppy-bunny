
ps = part_system_create();


// Glass
pt_glass = part_type_create();
part_type_shape(pt_glass, pt_shape_square);
part_type_size(pt_glass, .01, .05, 0, 0);
part_type_alpha1(pt_glass, .7);
part_type_speed(pt_glass, 2, 3, -.1, 0)
part_type_direction(pt_glass, 250, 290, 0, 0);
part_type_orientation(pt_glass, 0, 360, 0, 0, true);
part_type_life(pt_glass, seconds(60), seconds(61));
var c_blu = make_colour_rgb(162, 205, 220);
part_type_color1(pt_glass, c_blu);

//Blood
pt_blood = part_type_create();
part_type_shape(pt_blood, pt_shape_square);
part_type_blend(pt_blood, false);
part_type_size(pt_blood, .01, .05, 0, 0);
part_type_alpha1(pt_blood, 1);
part_type_speed(pt_blood, 1, 1.5, -.1, 0)
part_type_direction(pt_blood, 210, 350, 0, 0);
part_type_orientation(pt_blood, 0, 0, 0, 0, false);
part_type_life(pt_blood, seconds(4), seconds(4));
var c_blood = make_color_rgb(172, 50, 50);
part_type_color1(pt_blood, c_blood);

//Slime Blood
pt_slime_blood = part_type_create();
part_type_shape(pt_slime_blood, pt_shape_square);
part_type_blend(pt_slime_blood, false);
part_type_size(pt_slime_blood, .01, .05, 0, 0);
part_type_alpha1(pt_slime_blood, 1);
part_type_speed(pt_slime_blood, 1, 1.5, -.1, 0)
part_type_direction(pt_slime_blood, 210, 350, 0, 0);
part_type_orientation(pt_slime_blood, 0, 0, 0, 0, false);
part_type_life(pt_slime_blood, seconds(4), seconds(4));
var c_slime_blood = make_color_rgb(87, 198, 120);
part_type_color1(pt_slime_blood, c_slime_blood);
part_type_alpha1(pt_slime_blood, .7);

//Smoke
pt_smoke = part_type_create();
part_type_shape(pt_smoke, pt_shape_smoke);
part_type_blend(pt_smoke, false);
part_type_size(pt_smoke, .1, .15, 0, 0);
part_type_alpha2(pt_smoke, .7, 0);
part_type_speed(pt_smoke, 2, 3, -.1, 0)
part_type_direction(pt_smoke, 75, 115, 0, 0);
part_type_orientation(pt_smoke, 0, 360, 0, 0, true);
part_type_life(pt_smoke, seconds(.5), seconds(.7));
part_type_color2(pt_smoke, c_gray, c_dkgray);

//Fire Ball
pt_fireball = part_type_create();
part_type_shape(pt_fireball, pt_shape_square);
part_type_blend(pt_fireball, true);
part_type_size(pt_fireball, .04, .07, -.005, 0);
part_type_alpha1(pt_fireball, .5);
part_type_speed(pt_fireball, .01, .2, -.1, 0)
part_type_direction(pt_fireball, 45, 135, 0, 0);
part_type_orientation(pt_fireball, 0, 0, 0, 0, false);
part_type_life(pt_fireball, 8, 12);
part_type_color2(pt_fireball, c_blue, c_aqua);

//Fire
pt_fire = part_type_create();
part_type_sprite(pt_fire, spr_fire, 0, 0, 1);
part_type_size(pt_fire, .4, .7, -.05, 0);
part_type_orientation(pt_fire,0,360,2,0,0);
part_type_color2(pt_fire, c_blue, c_aqua);
part_type_alpha3(pt_fire, 1, 1, 0);
part_type_blend(pt_fire, 1);
part_type_direction(pt_fire, 85, 95, 0, 0);
part_type_speed(pt_fire, 2, 10, -.1, 0);
part_type_life(pt_fire, 25, 35);

//Ice Effect
pt_ice = part_type_create();
part_type_shape(pt_ice, pt_shape_square);
part_type_blend(pt_ice, true);
part_type_size(pt_ice, .015, .01, 0, 0);
part_type_alpha1(pt_ice, .7);
part_type_speed(pt_ice, 1, 1.5, -.1, 0);
part_type_direction(pt_ice, 210, 350, 0, 0);
part_type_orientation(pt_ice, 0, 0, 0, 0, false);
part_type_life(pt_ice, seconds(.2), seconds(.4));
part_type_color2(pt_ice, c_blue, c_aqua);

//Wood effect
pt_wood = part_type_create();
part_type_shape(pt_wood, pt_shape_square);
part_type_blend(pt_wood, false);
part_type_size(pt_wood, .01, .05, 0, 0);
part_type_alpha1(pt_wood, 1);
part_type_speed(pt_wood, 1, 1.5, -.1, 0)
part_type_direction(pt_wood, 210, 350, 0, 0);
part_type_orientation(pt_wood, 0, 0, 0, 0, false);
part_type_life(pt_wood, seconds(4), seconds(4));
var c_wood = make_color_rgb(113, 94, 94);
part_type_color1(pt_wood, c_wood);