visible = false;
phy_position_x = obj_car.x;
phy_position_y = obj_car.y;

if (obj_input.enter) {
    state = move_state;
    visible = true;
}

with (obj_car) {
    dir = point_direction(0, 0, obj_input.xaxis, obj_input.yaxis);
    //Get the length
    if (obj_input.xaxis == 0 and obj_input.yaxis == 0) {
        len = 0;
    } else {
        if (obj_player.able_to_move){
            len = 5;
        }
        get_face(dir);
    }
    
    //Get the hspd and vspd
        hspd = lengthdir_x (len, dir);
        vspd = lengthdir_y (len, dir);
    
    
    //Move
    if (obj_player.able_to_move == true) {
        phy_position_x += hspd;
        phy_position_y += vspd;
    }
}