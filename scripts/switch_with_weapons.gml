switch(weapon[i]) {

    case "Flamethrower":
        with(obj_player) {
        if gun != obj_flamethrower
        change_gun(obj_flamethrower);
        }
            break;
    case "Rifle":
        with(obj_player) {
        if gun != obj_ak47
        change_gun(obj_ak47);
        }
            break;
    case "Handgun":
        with(obj_player) {
        if gun != obj_handgun
        change_gun(obj_handgun);
        }
            break;
    case "Airstrike":
        with(obj_player) {
        if gun != obj_airstrike
        change_gun(obj_airstrike);
        }
            break;
    case "Melee":
        with(obj_player) {
        if gun != obj_melee
        change_gun(obj_melee);
        }
            break;
}