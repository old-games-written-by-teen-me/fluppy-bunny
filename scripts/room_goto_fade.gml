///room_goto_fade(fade_alpha, fade_speed, next_room)

var fade_speed = argument0;
var next_room = argument1;

var fade = instance_create(x, y, obj_fade_black);
fade.fade_speed = fade_speed;
fade.next_room = next_room;