///change_gun(new_gun)
var new_gun = argument0;
with (gun) {
    instance_destroy();
}
gun = instance_create(x, y, new_gun);
gun.creator = id;