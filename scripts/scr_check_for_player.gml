///scr_check_for_player
if (instance_exists(obj_player)) {
    var dis = point_distance (x, y, obj_player.x, obj_player.y);
    if (dis < sight && !collision_line(x, y, obj_player.x, obj_player.y, obj_wall, obj_system.quality, true)) {
        target = obj_player;
        state = scr_enemy_chase_state;
        var dir = point_direction(x, y, target.x, target.y);
        xaxis = lengthdir_x(1, dir);
        yaxis = lengthdir_y(1, dir);
        spd = chase_spd;
    } else {
        scr_enemy_choose_next_state();
        target = noone;
    }
} else {
    scr_enemy_choose_next_state();
}