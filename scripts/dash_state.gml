movement = DASH;
//Dash
{
    get_face(dir);
    var mouse_d = point_direction(x, y, mouse_x, mouse_y);
    /*if (round(mouse_dir/90) != RIGHT) {
        face = round(mouse_dir/90);
    }*/
    
    len = spd*1.6;
    
    //Get the length
    //Get the hspd and vspd
    hspd = lengthdir_x(len, mouse_dir);
    vspd = lengthdir_y(len, mouse_dir);
    
    //Move
    if (able_to_move == true) {
        phy_position_x += hspd;
        phy_position_y += vspd;
    }
}