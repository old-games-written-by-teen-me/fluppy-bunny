move_right = keyboard_check(ord('D'));
move_left = keyboard_check(ord('A'));
move_up = keyboard_check(ord('W'));
move_down = keyboard_check(ord('S'));
run = keyboard_check(vk_lshift);
fire = mouse_check_button(mb_left);
fire2 = mouse_check_button(mb_right);
fire2_released = mouse_check_button_released(mb_right);
dash = keyboard_check_pressed(vk_lcontrol);
enter = keyboard_check_pressed(ord('E'));
tab = keyboard_check(vk_tab);
brake = keyboard_check(vk_space);
aim = mouse_check_button(mb_right);
stand_by = mouse_check_button_pressed(mb_middle);
frag = keyboard_check_pressed(ord('F'));

xaxis = move_right - move_left;
yaxis = move_down - move_up;
