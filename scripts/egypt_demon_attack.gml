///egypt_demon_attack()
if (instance_exists(obj_loot_barrel)) {
    var barr = instance_nearest(x, y, obj_loot_barrel);
    if (!collision_line(target.x, target.y, barr.x, barr.y, obj_lifeform_parent, false, false)) {
        if (distance_to_object(barr) < sight) {
            var dir = point_direction(target.x, target.y, barr.x, barr.y);
        } else {
            var dir = point_direction (x, y, target.x, target.y);
        }
    } else {
        var dir = point_direction(x, y, target.x, target.y);
    }
} else {
    var dir = point_direction (x, y, target.x, target.y);
}
var xforce = lengthdir_x(450, dir);
var yforce = lengthdir_y(450, dir);

obj_view.shake = 5;
target.stats.hp -= damage;
with (target)
physics_apply_local_impulse(x, y, xforce, yforce);
