///create_explosion(x, y, size)

var xx = argument0;
var yy = argument1;
var size = argument2;
size = round(size);

with (obj_view) {
    shake += size;
}

repeat (3*size) {
    instance_create(xx+random_range(-8, 8)*size, yy+random_range(-8, 8)*size, obj_explosion);
}